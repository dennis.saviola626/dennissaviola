<?php

namespace App\Http\Controllers;
use App\Models\posts_users;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;

class AjaxController extends Controller
{
    public function ajax_test(){
      return view('ajax_view');
    }

    public function store_user(Request $req){

      $name = $req->user_name;
      $email = $req->email;
      $password = $req->password;

      // validation
      $exist = posts_users::where('email', $email)->exists();

      if ($exist) {
        return [
          'status' => 0,
          'message' => 'Email exist',
        ];
      }

      posts_users::insert([
        'name' => $name,
        'password' => Hash::make($password),
        'email' => $email,
      ]);

      return [
        'status' => 1,
        'message' => 'Success',
      ];
    }

    public function display_user(){
      $all_post_user = posts_users::get();


      return view('user_list')->with(['user_list' => $all_post_user]);
    }
}
