<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;
use App\Models\password_resets;
use Illuminate\Support\Facades\Hash;
use App\Mail\ResetPasswordMail;
use Illuminate\Support\Facades\Mail;
use Illuminate\Notifications\Messages\MailMessage;


class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    public function sendResetLinkEmail(request $request){

      $password_reset = new password_resets;
      $token = Hash::make($request-> email);
      $token = str_replace("/","",$token);

      $password_reset->token = $token;
      $password_reset->email = $request-> email;
      $password_reset->save();

      ## send the reset password link to emails
      $link = ('http://localhost:8000/') . 'password/reset/' . $token;

Mail::to("dennis.saviola626@gmail.com")->send(new ResetPasswordMail($link));


    }

}
