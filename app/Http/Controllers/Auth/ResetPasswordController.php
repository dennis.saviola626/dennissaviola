<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\ResetsPasswords;
use App\Models\password_resets;
use Illuminate\Http\Request;
use App\Models\posts_users;
use Illuminate\Support\Facades\Hash;



class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;


    public function showResetForm($token){
      $exist = password_resets::where('token',$token)->first();

      if($exist){
        $email = $exist->email;
        $userid = posts_users::where('email', $email)->value('id');
        password_resets::where('token',$token)->update([
          'user_id' => $userid,

        ]);
        return view('auth.passwords.reset')->with(['token'=>$token, 'user_id' => $userid, 'email'=>$email]);
      }else{
        dd($exist);
      }


    }

    public function reset(request $request){
      $this->validate($request, [
        'password'=> ['required','min:8','regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*(_|[^\w])).+$/'],
        'password_confirmation' => ['required', 'same:password']
      ]);

      $user_id = $request->user_id;
      $user = posts_users::find($user_id);

      if ($user) {
        // update
        $passcode = $request->password;
        posts_users::where('id',$user_id)->update([
          'password' => Hash::make($passcode),
        ]);
      }
    }


}
