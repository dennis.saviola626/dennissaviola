<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Comment;
use App\Models\Post;
use Session;
use Carbon;

class CommentsController extends Controller
{
    public function __construct(){
      $this->middleware('auth', ['except'=>'store']);
    }


    public function store(Request $request, $post_id)
    {
      $this->validate($request, array(
      'name'        => 'required|max:255',
      'email'        => 'required|email|max:255',
      'comment'        => 'required|min:5|max:2000',
      ));
      $post = Post::find($post_id);

      $comment = new Comment();
      $comment->name = $request->name;
      $comment->email = $request->email;
      $comment->comment = $request->comment;
      $comment->approved = true;
      $comment->post()->associate($post);

      $comment->save();
      Session::flash('success', 'Comment was added');
      return redirect()->route('blog.single', [$post->slug]);
    }


    public function edit($id)
    {
        $comment = Comment::find($id);
        return view('comments.edit')->with(['comment' => $comment]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $comment= Comment::find($id);

        $this->validate($request, array('comment'=>'required'));

        $comment->comment = $request->comment;
        $comment->save();

        Session::flash('Success', 'Comment updated');

        return redirect()->route('posts.show', $comment->post->id);
    }

    public function delete($id){
      $comment = Comment::find($id);
      return view('comments.delete')->with(['comment' => $comment]);
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $comment = Comment::find($id);
      $post_id = $comment->post->id;
      $comment -> delete();

      Session::flash('success', 'Deleted Comment');
      return redirect()->route('posts.show', $post_id);
    }

    public function display_commentajax($slug){

      $post = Post::where('slug', '=', $slug)->first();
      return view('blog.comment_table')->with(['post' => $post]);
    }

    public function store_comments(Request $req){

      $name = $req->name;
      $email = $req->email;
      $comment = $req->comment;
      $postid = $req->post_id;

      Comment::insert([
        'name' => $name,
        'email' => $email,
        'approved'=>1,
        'post_id' =>$postid,
        'comment'=>$comment,

      ]);
      return [
        'status' => 1,
        'message' => 'Success',
      ];
    }
}
