<?php
namespace App\Http\Controllers;

use App\Models\Post;

class PagesController extends Controller {

  public function getIndex() {
    $posts = Post::orderBy('created_at', 'desc')->limit(4)->get();
    return view('pages.welcome')->withPosts($posts);
  }

  public function getAbout() {
    $first = "Dennis";
    $last = "Saviola";

    $full = $first . " " . $last;
    $email = "dennis@hippo.com";
    $data = [];
    $data['fullname'] = $full;
    $data['email'] = $email;
    return view('pages/about')->withData($data);
  }

  public function getContact(){
    return view("pages.contact");
  }

}
