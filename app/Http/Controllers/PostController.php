<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Post;
use Session;
use App\Http\Controllers\Input;
use App\Models\Category;
use App\Models\Tag;
use App\Models\Post_Tagging;
use Image;

class PostController extends Controller
{

    public function __construct(){
      $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $posts = Post::orderby('id', 'desc')->paginate(3);
      return view('posts.index')->withPosts($posts);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

      $categories = Category::all();

      $tags = Tag::all();


      return view('posts.create')->with(['categories'=> $categories])->with(['tags'=> $tags]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // validate the data
        $this->validate($request, array(
        'title'       => 'required|max:255',
        'slug'        => 'required|alpha_dash|min:5|max:255|unique:posts,slug',
        'body'        => 'required',
        ));
        // Store in the database
        $post = new Post;

        $post->title = $request->title;
        $post->slug = $request->slug;
        $post->category_id = $request->category_id;
        $post->body = $request->body;
        //dd($request->tags);

        if($request->hasFile('featured_image')){
          $image = $request ->file('featured_image');
          $filename = time() . '.' . $image->getClientOriginalExtension();
          $location = public_path('images/' . $filename);
          Image::make($image)->resize(800, 400)->save($location);

          $post->image = $filename;
        }

        $post->save();

        ##
        $post->tags()->sync($request->tags, false);

        //$post->tags()->sync($request->tags, false);
        Session::flash('success', 'The blog is successfully saved!');

        // redirect to another page
        return redirect()->route('posts.show', $post->id);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Post::find($id);
        $post_tag = Post_Tagging::where('post_id', $id)->get();
        // [2,3,4]
        $tag = Tag::whereIn('id',$post_tag->pluck('tag_id'))->get();

        return view('posts/show')->with(['post' => $post , 'tags' => $tag]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $post = Post::find($id);
      $categories = Category::all();
      $cats = array();
      foreach($categories as $category){
        $cats[$category->id] = $category->name;
      }

        $tags = Tag::all();
        $tags2 = array();
        foreach ($tags as $tag) {
            $tags2[$tag->id] = $tag->name;

        }
      return view('posts.edit')->withPost($post)->with(['categories'=> $cats, 'tags'=>$tags2]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $post = Post::find($id);
      if ($request->input('slug') == $post->slug ) {
        $this->validate($request, array(
          'title' => 'required|max:255',
          'category_id'=>'required|integer',
          'body' => 'required'
          ));
      } else {
          $this->validate($request, array(
          'title' => 'required|max:255',
          'slug' => 'required|alpha_dash|min:5|max:255|unique:posts,slug',
          'category_id'=>'required|integer',
          'body' => 'required'
          ));
      }
      $post = Post::find($id);

      $post->title = $request->input('title');
      $post->slug = $request->input('slug');
      $post->category_id = $request->input('category_id');
      $post->body = $request->input('body');

      $post->save();
      if (isset($request->tags)) {
            $post->tags()->sync($request->tags);
        } else {
            $post->tags()->sync(array());
        }

      Session::flash('success', 'This post was successfully saved.');

      return redirect()->route('posts.show', $post->id);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $post = Post::find($id);
      $post->tags()->detach();

      $post ->delete();
      Session::flash('success', "The post was successfully deleted.");
      return redirect()->route('posts.index');
    }


    public function search(request $request){
      $q = $request-> searchkey;
    $posts = Post::where('Title','LIKE','%'.$q.'%')->paginate(3);
    if(count($posts) > 0)
        return view('posts.index')->with(['posts'=>$posts])->withQuery ( $q );
    else
    Session::flash('notfound', 'Please try again, No data was matched on the database.');
    return view ('posts.index')->with(['posts'=>$posts]);

    }
}
