<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Tag;
use App\Models\Post_Tagging;
use App\Models\Post;

use Session;



class TagController extends Controller
{

    public function __construct(){
      $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tags = Tag::all();

        return view('tags.index')->with(['tags' => $tags]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     public function store(Request $request)
         {
             $this->validate($request, array('name' => 'required|max:255'));
             $tag = new Tag;
             $tag->name = $request->name;
             $tag->save();

             Session::flash('success', 'New Tag was successfully created!');

             return redirect()->route('tags.index');
         }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $tag = Tag::find($id);
        $tag_count = $tag->posts_count($id);
        $tagging_postid = Post_Tagging::where('tag_id',$id)->pluck('post_id');

        $postdata = Post::whereIn('id', $tagging_postid)->get();

        foreach ($postdata as $key => $value) {
          $tagfrompost = Post_Tagging::where('post_id', $value->id)->pluck('tag_id');
          $tag_name = Tag::whereIn('id', $tagfrompost)->pluck('name');
          $value['tag_id'] = $tag_name;


        }


        return view('tags.show')->with(['tag'=> $tag , 'tag_count' => $tag_count, 'post_data'=>$postdata]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tag = Tag::find($id);
        return view('tags.edit')->with(['tag' => $tag]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $tag = Tag::find($id);
        $this->validate($request, ['name'=>'required|max:255']);

        $tag->name = $request->name;
        $tag->save();

        Session::flash('success','Successfully saved your new tag');
        return redirect()->route('tags.show', $tag->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $tag = Post_Tagging::where('tag_id', $id)->delete();
        $post = Tag::where('id', $id)->delete();
        Session::flash('Success', 'Tag was deleted successfully');
        return redirect()->route('tags.index');
    }
}
