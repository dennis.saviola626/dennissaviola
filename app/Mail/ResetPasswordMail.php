<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ResetPasswordMail extends Mailable
{
    use SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
     public $link;
    public function __construct($link)
    {
        $this->link = $link;

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
      $this->email='dennis.saviola626@gmail.com';
      return $this->from($this->email)
                      ->markdown('mails')
                      ->with([
                        'link' => $this->link,
                      ]);
    }
}
