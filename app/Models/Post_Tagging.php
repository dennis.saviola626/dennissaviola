<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Post_Tagging extends Model
{
    use HasFactory;

    protected $table ='posts_tagging';

    public function post(){
      return $this->belongsToMany('App\Models\Post','posts','tag_id','post_id');
    }

    public function tags(){
      return $this->belongsToMany('App\Models\Tag','posts_tags');
    }

}
