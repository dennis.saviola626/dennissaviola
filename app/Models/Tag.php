<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Post_Tagging;


class Tag extends Model
{
    use HasFactory;

    protected $table ='posts_tags';

    public function posts_count($id){
      $count = 0;

      $count = Post_Tagging::where('tag_id', $id)->count();

      return $count;

    }

}
