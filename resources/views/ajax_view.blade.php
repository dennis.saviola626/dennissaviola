@extends('main')

@section('title', '| AJAX TEST')

@section('content')
<meta name="csrf-token" content="{{ csrf_token() }}">
<div class="row">
  <div class="col-md-10">
    <h1>AJAX TEST</h1>

    <span class="user_list"></span>

    <form class="ajax_form" action="index.html" method="post">
      @csrf
      <input type="text" name="user_name" value="" class="user_name" placeholder="Name">
      <input type="text" name="email" value="" class="email" placeholder="email">
      <input type="text" name="password" value="" class="password" placeholder="password">

      <button type="button" name="button" class="submit_button">SUBMIT</button>
    </form>

  </div>



</div>


@endsection
<script
  src="https://code.jquery.com/jquery-3.6.1.min.js"
  integrity="sha256-o88AwQnZB+VDvE9tvIXrMQaPlFFSUTR+nldQm1LuPXQ="
  crossorigin="anonymous"></script>

<script type="text/javascript">

  function display_user(){

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
      url: '/display_user',
      type: "GET",
      processData: false,
      contentType: false,
      success:function(data) {
        $('.user_list').html(data)
        console.log(data)
      },
      error: function(error) {
          console.log('eror',error.responseText)
      }
    });

  }

  $(function (){
      display_user()

      $(document).on('click', '.submit_button', function () {
          var value = $('.user_name').val()
          var email = $('.email').val()
          var password = $('.password').val()

          var fd = new FormData();
          fd.append("user_name", value);
          fd.append("email", email);
          fd.append("password", password);

          $.ajaxSetup({
              headers: {
                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              }
          });
          $.ajax({
            url: '/store_user',
            type: "POST",
            data: fd,
            processData: false,
            contentType: false,
            success:function(data) {
              if (data['status'] == 0) {
                alert(data['message'])
                return;
              }

              display_user()
              $('.user_name').val('')
              $('.email').val('')
              $('.password').val('')


            },
            error: function(error) {
                console.log('eror',error.responseText)
            }
          });

          console.log(value)
      });



  });


</script>
