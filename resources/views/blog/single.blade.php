@extends('main')
<?php $titleTag = htmlspecialchars($post->title); ?>
@section('title', "| $titleTag")

@section('content')
<meta name="csrf-token" content="{{ csrf_token() }}">

	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			@if(!empty($post->image))
				<img src="{{asset('/images/' . $post->image)}}" width="800" height="400" />
			@endif
			<h1>{{ $post->title }}</h1>
			<p>{!! $post->body !!}</p>
			<hr>
			<p>Posted In: {{ $post->category->name }}</p>
		</div>
	</div>

	<span class="comment_list"></span>

	<div class="row">
		<div id="comment-form" class="col-md-8 col-md-offset-2" style="margin-top: 50px;">
			{{ Form::open(['route' => ['comments.store', $post->id], 'method' => 'POST']) }}

				<div class="row">
					<div class="col-md-6">
						{{ Form::label('name', "Name:") }}
						{{ Form::text('name', null, ['class' => 'form-control name']) }}
					</div>

					<div class="col-md-6">
						{{ Form::label('email', 'Email:') }}
						{{ Form::text('email', null, ['class' => 'form-control email']) }}
					</div>

					<div class="col-md-12">
						{{ Form::label('comment', "Comment:") }}
						{{ Form::textarea('comment', null, ['class' => 'form-control comment-text', 'rows' => '5']) }}

						<button type="button" name="button" class="btn btn-success btn-block submitbtn">SUBMIT</button>

					</div>
				</div>

			{{ Form::close() }}
		</div>
	</div>

@endsection
<script
  src="https://code.jquery.com/jquery-3.6.1.min.js"
  integrity="sha256-o88AwQnZB+VDvE9tvIXrMQaPlFFSUTR+nldQm1LuPXQ="
  crossorigin="anonymous"></script>

<script type="text/javascript">
	displayComment()
	function displayComment(){

		$.ajaxSetup({
				headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}
		});

		$.ajax({
			url: '/display_commentajax/'+`{{ $slug }}`,
			type: "GET",
			processData: false,
			contentType: false,
			success:function(data) {
				$('.comment_list').html(data)

			},
			error: function(error) {
					console.log('eror',error.responseText)
			}
		});

	}
	$(function (){

      $(document).on('click', '.submitbtn', function () {
          var value = $('.name').val()
          var email = $('.email').val()
          var comment = $('.comment-text').val()

					var post_id = `{{ $postid }}`

          var fd = new FormData();
          fd.append("name", value);
          fd.append("email", email);
          fd.append("comment", comment);
					fd.append("post_id", post_id);

          $.ajaxSetup({
              headers: {
                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              }
          });
					$.ajax({
						url: '/store_comments',
						type: "POST",
						data: fd,
						processData: false,
						contentType: false,
						success:function(data) {
							if (data['status'] == 1) {
								displayComment()
								$('.name').val('')
								$('.email').val('')
								$('.comment').val('')
								return;
							}
						},
						error: function(error) {
								console.log('eror',error.responseText)
						}
					});
				});
			});

</script>
