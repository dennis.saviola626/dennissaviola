@extends('main')

@section('title', '| All Categories')

@section('content')

  <div class="row">
    <div class="col-md-8">
      <h1>Categories</h1>
      <table class="table">
        <thead>
          <tr>
            <th>#</th>
            <th>Name</th>
          </tr>
        </thead>
        <tbody>
          @foreach ($categories as $key => $category)
          <tr>
            <th>{{$key + 1}}</th>
            <td>{{$category -> name}}</td>
          @endforeach
          </tr>
        </tbody>
      </table>
    </div>

    <div class="col-md-3">
      <div class="well">
        {!! Form::open(['route' => 'categories.store', 'method'=>'POST']) !!}
        <h2>New Category</h2>
        {{ Form::label('name', 'Name: ') }}
        {{ Form::text('name', null, ['class' => 'form-control']) }}
        {{ Form::submit('Create new Category', ['class'=>'btn btn-primary btn-block btn-h1-spacing']) }}

        </div>

      </div>

    </div>
  </div>

@endsection
