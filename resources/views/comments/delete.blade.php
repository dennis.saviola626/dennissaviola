@extends('main')

@section('title', "| Delete Content")

@section('content')

  <div class="row">
    <div class="cold-md-8 cold-md-offset-2">
      <h1>DELETE THIS COMMENT?</h1>
      <p>
        <strong>Name: </strong>{{ $comment->name }}<br>
        <strong>Email: </strong>{{ $comment->email }}<br>
        <strong>Comment: </strong>{{ $comment->comment }}<br>
      </p>

      {{ Form::open(['route'=>['comments.destroy', $comment->id], 'method'=>'DELETE']) }}
        {{ Form::submit('Yes Delete This Comment', ['class'=>'btn btn-lg btn-block btn-danger']) }}
      {{ Form::close() }}

    </div>
  </div>

@endsection
