@component('mail::message')
<a href="{{ $link }}">Click to reset password</a>
@endcomponent
