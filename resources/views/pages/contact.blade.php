@extends('main')

@section('title', '| Contact Us')

@section('content')
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <h1>Contact Us</h1>
          <div class="form-group">
            <label name="email">Email:</label>
            <input id="email" name="email" class="form-control">
            <div class="form-group">
              <label name="email">Subject:</label>
              <input id="Subject" name="Subject" class="form-control">
              <div class="form-group">
                <label name="email">Message:</label>
                <textarea id="Message" name="Message" class="form-control">Type your message here:    </textarea>
              </div>

              <input type="submit" value="Send Message" class="btn btn-success">
            </div>
          </div>
        </div>
      </div>
    </div>

@endsection
