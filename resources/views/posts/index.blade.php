@extends('main')

@section('title', '| All Posts')

@section('content')

<div class="row">
  <div class="col-md-10">
    <h1>All Posts</h1>
  </div>
      <form action="/search" method="POST" role="search">
        {{ csrf_field() }}
        <div class="input-group">
            <input type="text" class="form-control" name="searchkey"
                placeholder="Search Title"> <span class="input-group-btn">
                <button type="submit" class="btn btn-default">
                    <span class="glyphicon glyphicon-search"></span>
                </button>
            </span>
        </div>
    </form>
  <div class="col-md-2">
    <a href="{{ route('posts.create') }}" class ="btn btn-lg btn-block btn-primary btn-h1-spacing">
      Create new post</a>
  </div>
  <div class="col-md-12">
    <hr>
  </div>
</div>

<div class="row">
  @if(session()->has('notfound'))
      <div class="alert alert-danger">
          {{ session()->get('notfound') }}
      </div>
  @endif
  <div class="col-md-12">
    <table class="table">
      <thead>
        <th>#</th>
        <th>Title</th>
        <th>Body</th>
        <th>Created At</th>

        <th></th>
      </thead>

      <tbody>

        @foreach ($posts as $key =>  $post)
          <tr>
            <th>{{ $key + 1 }}</th>
            <td>{{ $post->title}}</td>
            <td>{{ substr($post->body, 0, 100) }}{{ strlen($post->body) > 5 ? "..." : "" }}</td>
            <td>{{ $post->created_at}}</td>
            <td>
              <a href="{{ route('posts.show', $post->id) }}" class="btn btn-default btn-sm">View</a>
              <a href="{{ route('posts.edit', $post->id) }}" class="btn btn-default btn-sm">Edit</a>
            </td>
          </tr>
        @endforeach
      </tbody>
    </table>

    <div class="text-center">
        {!! $posts->links(); !!}
    </div>
  </div>
</div>


@endsection
