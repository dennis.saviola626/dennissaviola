<table class="table table-bordered">
  <thead>
    <th>Name</th>
    <th>Email</th>

  </thead>
  <tbody>
    @foreach($user_list as $key => $data)
    <tr>
      <td>{{ $data->name }}</td>
      <td>{{ $data->email }}</td>

    </tr>
    @endforeach
  </tbody>
</table>
