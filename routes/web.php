<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PagesController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\BlogController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\TagController;
use App\Http\Controllers\CommentsController;
use App\Http\Controllers\AjaxController;





/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['middleware' => ['web']], function(){

Route::get('blog/{slug}', [BlogController::class,'getSingle'])->name('blog.single')->where('slug', '[\w\d\-\_]+');
Route::get('blog', [BlogController::class,'getIndex'])->name('blog.index');
Route::get('contact', [PagesController::class,'getContact']);
Route::get('about', [PagesController::class,'getAbout']);
Route::get('/', [PagesController::class,'getIndex']);
Route::resource('posts', PostController::class);
Route::post('search', [PostController::class,'search'])->name('search');

//comments routes
Route::post('comments/{post_id}', [CommentsController::class,'store'])->name('comments.store');
Route::get('comments/{id}/edit', [CommentsController::class,'edit'])->name('comments.edit');
Route::put('comments/{id}', [CommentsController::class,'update'])->name('comments.update');
Route::delete('comments/{id}', [CommentsController::class,'destroy'])->name('comments.destroy');
Route::get('comments/{id}/delete', [CommentsController::class,'delete'])->name('comments.delete');




//categories routes
Route::resource('categories', CategoryController::class)->except(['create']);


//tags routes
Route::resource('tags', TagController::class)->except(['create']);

});

Auth::routes();
Route::get('logout', [LoginController::class,'logout'])->name('logout');
Route::get('auth/login', [HomeController::class,'getLogin']);

##ajax comment
Route::get('display_commentajax/{slug}', [CommentsController::class,'display_commentajax'])->name('display_commentajax');
Route::post('store_comments', [CommentsController::class,'store_comments'])->name('store_comments');


## ajax test
Route::get('ajax_test', [AjaxController::class,'ajax_test'])->name('ajax_test');
Route::post('store_user', [AjaxController::class,'store_user'])->name('store_user');
Route::get('display_user', [AjaxController::class,'display_user'])->name('display_user');
